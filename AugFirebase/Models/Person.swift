//
//  Person.swift
//  AugFirebase
//
//  Created by Dariia Pavlovska on 07.08.2022.
//

import Foundation

struct Person: Codable {
    var name: String
    var number: String
}
