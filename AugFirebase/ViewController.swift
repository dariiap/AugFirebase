//
//  ViewController.swift
//  AugFirebase
//
//  Created by Dariia Pavlovska on 07.08.2022.
//

import UIKit
import FirebaseDatabase

class ViewController: UIViewController {

    let tableView = UITableView()
    var persons: [Person] = []
    
    let database = Database.database(url: "https://august-firebase-b9b90-default-rtdb.europe-west1.firebasedatabase.app/").reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Firebase test"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(
            title: "Add New",
            style: .done,
            target: self,
            action: #selector(addPerson))
        view.backgroundColor = .white
        setupSubviews()
    }

    private func setupSubviews() {
        view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        fetchPersons()
    }
    
    func fetchPersons() {

        var tempArray = [Person]()
        database.observe(.value, with: { snapshot in
            if let retrievedDict = snapshot.value as? NSDictionary {
                for (_, value) in retrievedDict {
                    if let innerDictionary = value as? [String : Any] {
                        let name = innerDictionary["name"] as! String
                        let number = innerDictionary["number"] as! String
                        tempArray.append(Person(name: name, number: number))
                    }
                }
                print(snapshot.value as Any)
            }
            self.persons = tempArray
            self.tableView.reloadData()
        })
    }

    @objc func addPerson() {
        let alert = UIAlertController(title: "Add Person", message: "new name", preferredStyle: .alert)
        alert.addTextField()
        
        let submitButton = UIAlertAction(title: "Add", style: .default) { (action) in
            
            let nameTextField = alert.textFields![0]
            let number = Int.random(in: 0...100)
            
            let object = [ "name" : nameTextField.text ?? "",
                           "number" : String(number)]

            self.database.child("\(number)").setValue(object)
            self.fetchPersons()
        }
        alert.addAction(submitButton)
        self.present(alert, animated: true, completion: nil)
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = persons[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let alert = UIAlertController(title: "Edit Person", message: "Edit name", preferredStyle: .alert)
        alert.addTextField()

        let textField = alert.textFields![0]
        textField.text = self.persons[indexPath.row].name

        let saveButton = UIAlertAction(title: "Save", style: .default) { (action) in
            let textField = alert.textFields![0]
            let object = [ "name" : textField.text ?? "",
                           "number" : self.persons[indexPath.row].number]

            self.database.child(self.persons[indexPath.row].number).setValue(object)
            self.fetchPersons()
        }

        alert.addAction(saveButton)
        self.present(alert, animated: true, completion: nil)
    }
    
    internal func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
      if editingStyle == .delete {
          print("Deleted")
          database.child(persons[indexPath.row].number).removeValue()
          fetchPersons()
      }
    }
}

